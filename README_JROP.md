# Instructions

## Installation

```sh
./util/qmk_install.sh
git submodule update --init
```

## Building/Flashing

Run `make jrop`, which will run the following commands:

```
cd keyboards/dz60/keymaps/jrop/
qmk json2c -o keymap.c keymap.json
cd -
qmk compile -kb dz60 -km jrop
qmk flash -kb dz60 -km jrop -bl dfu
```

Note: if the flasher says `Bootloader not found...`, then hold down the reset
button on the keyboard for ~10 seconds.
